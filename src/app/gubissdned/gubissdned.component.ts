import { Component, OnInit } from '@angular/core';
import { GetRecordsByDayService } from 'src/app/services/get-records-by-day.service'

@Component({
  selector: 'app-gubissdned',
  templateUrl: './gubissdned.component.html',
  styleUrls: ['./gubissdned.component.scss']
})
export class GUBISSDNEDComponent implements OnInit {

  public datos :any;
  constructor(private getRecordsByDayService : GetRecordsByDayService) { }

  ngOnInit() {
  }

  async fnForm(data){
    console.log(data);
    this.datos = await this.getRecordsByDayService.fnPostRedordsByIsStrtDEndD(data.is,data.strtD,data.endD);
    console.log("DATOS",this.datos);

  }

}
