import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GUBISSDNEDComponent } from './gubissdned.component';

describe('GUBISSDNEDComponent', () => {
  let component: GUBISSDNEDComponent;
  let fixture: ComponentFixture<GUBISSDNEDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GUBISSDNEDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GUBISSDNEDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
