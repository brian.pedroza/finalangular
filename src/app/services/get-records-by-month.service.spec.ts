import { TestBed } from '@angular/core/testing';

import { GetRecordsByMonthService } from './get-records-by-month.service';

describe('GetRecordsByMonthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetRecordsByMonthService = TestBed.get(GetRecordsByMonthService);
    expect(service).toBeTruthy();
  });
});
