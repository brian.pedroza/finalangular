import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configuration } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  constructor(private http: HttpClient, public config: Configuration) { 
    
  }
  readonly strUrl: string = this.config.apiUrl;
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  fnUpdateDBXmlsns(){
    console.log("ENTRA AL SERVICE fnUpdateDBXmlsns", this.strUrl);
    return this.http.get<any>(`${this.strUrl}/update`,this.httpOptions).toPromise();
  }
}
