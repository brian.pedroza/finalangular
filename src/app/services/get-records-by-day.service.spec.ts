import { TestBed } from '@angular/core/testing';

import { GetRecordsByDayService } from './get-records-by-day.service';

describe('GetRecordsByDayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetRecordsByDayService = TestBed.get(GetRecordsByDayService);
    expect(service).toBeTruthy();
  });
});
