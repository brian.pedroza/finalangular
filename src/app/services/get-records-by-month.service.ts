import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configuration } from '../config/config';
@Injectable({
  providedIn: 'root'
})
export class GetRecordsByMonthService {

  constructor(private http: HttpClient, public config: Configuration) { 
    
  }
  readonly strUrl: string = this.config.apiUrl;
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  fnGetRecordByIsMonth(is,month){
    let isstr = is;
    console.log("ENTRA AL SERVICE fnGetRecordByIsMonth", this.strUrl, is);
    return this.http.get<any>(`${this.strUrl}/users/${isstr}/${month}`).toPromise();
  }
  fnGetRecordsByMonth(month){
    console.log("ENTRA AL SERVICE fnGetRecordByIsMonth", this.strUrl);
    return this.http.get<any>(`${this.strUrl}/period/${month}`).toPromise();
  }
}
