import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Directive } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { DaysComponent} from './days/days.component';
import { GUBSDNEDComponent } from './gubsdned/gubsdned.component';
import { GUBISSDNEDComponent } from './gubissdned/gubissdned.component';
import { GRBISMComponent } from './grbism/grbism.component';
import { GRBMComponent } from './grbm/grbm.component';
import { UpdateComponent } from './update/update.component';


@NgModule({
  declarations: [
    AppComponent,
    DaysComponent,
    GUBSDNEDComponent,
    GUBISSDNEDComponent,
    GRBISMComponent,
    GRBMComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
