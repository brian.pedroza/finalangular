import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GRBMComponent } from './grbm.component';

describe('GRBMComponent', () => {
  let component: GRBMComponent;
  let fixture: ComponentFixture<GRBMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GRBMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GRBMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
