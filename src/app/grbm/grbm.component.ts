import { Component, OnInit } from '@angular/core';
import { GetRecordsByMonthService } from 'src/app/services/get-records-by-month.service';

@Component({
  selector: 'app-grbm',
  templateUrl: './grbm.component.html',
  styleUrls: ['./grbm.component.scss']
})
export class GRBMComponent implements OnInit {

  public datos: any;
  constructor(private getRecordsByMonthService : GetRecordsByMonthService) { }

  ngOnInit() {
  }

  async fnForm(data){
    console.log(data);
    this.datos = await this.getRecordsByMonthService.fnGetRecordsByMonth(data.month);
    console.log("DATOS",this.datos);

  }

}
