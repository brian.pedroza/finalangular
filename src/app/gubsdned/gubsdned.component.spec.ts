import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GUBSDNEDComponent } from './gubsdned.component';

describe('GUBSDNEDComponent', () => {
  let component: GUBSDNEDComponent;
  let fixture: ComponentFixture<GUBSDNEDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GUBSDNEDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GUBSDNEDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
