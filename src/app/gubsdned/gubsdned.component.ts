import { Component, OnInit } from '@angular/core';
import { GetRecordsByDayService } from 'src/app/services/get-records-by-day.service'

@Component({
  selector: 'app-gubsdned',
  templateUrl: './gubsdned.component.html',
  styleUrls: ['./gubsdned.component.scss']
})
export class GUBSDNEDComponent implements OnInit {

  public datos: any;

  constructor(private getRecordsByDayService : GetRecordsByDayService) { }

  ngOnInit() {
  }

  async fnForm(data){
    console.log(data);
    this.datos = await this.getRecordsByDayService.fnPostRecordsByStrtDEndD(data.strtD,data.endD);
    console.log("DATOS",this.datos);

  }

}
