import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DaysComponent} from './days/days.component';
import { GUBSDNEDComponent } from './gubsdned/gubsdned.component';
import { GUBISSDNEDComponent } from './gubissdned/gubissdned.component';
import { GRBISMComponent} from './grbism/grbism.component';
import { GRBMComponent} from './grbm/grbm.component';
import { UpdateComponent} from './update/update.component';


const routes: Routes = [
  {
    path: '',

    children: [
        {
          path: 'days',
          component : DaysComponent
        },
        {
          path: 'StrtNEndDate',
          component : GUBSDNEDComponent
        },
        {
          path: 'IsStrtNEndDate',
          component : GUBISSDNEDComponent
        },
        {
          path: 'IsMonth',
          component : GRBISMComponent
        },
        {
          path: 'Month',
          component : GRBMComponent
        },
        {
          path: 'Update',
          component : UpdateComponent
        },
      ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
