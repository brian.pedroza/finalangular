import { Component, OnInit } from '@angular/core';
import { UpdateService } from 'src/app/services/update.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  public datos : any;
  constructor(private updateService : UpdateService) { }

  ngOnInit() {
    this.fnUpdate();
  }

  async fnUpdate(){
    this.datos = await this.updateService.fnUpdateDBXmlsns();
    console.log("DATOS",this.datos);

  }

}
