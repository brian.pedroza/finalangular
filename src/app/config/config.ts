import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class Configuration{
    apiUrl:string;

    constructor(){
        this.apiUrl = environment.apiUrl;
    }
}