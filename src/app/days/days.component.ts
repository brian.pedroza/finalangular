import { Component, OnInit } from '@angular/core';
import { GetRecordsByDayService } from 'src/app/services/get-records-by-day.service'

@Component({
  selector: 'app-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.scss']
})
export class DaysComponent implements OnInit {

  public datos: any;

  constructor(private getRecordsByDayService: GetRecordsByDayService) { }

  ngOnInit() {
    this.fnGetDaysService();
  }

  async fnGetDaysService(){
    console.log("ENTRA AL DAYS");
    this.datos = await this.getRecordsByDayService.fnGetAllRecordsDay();
    console.log("DATOS",this.datos);
  }



}
